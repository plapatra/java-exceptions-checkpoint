package com.galvanize;

import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {

    // write your tests here
    @Test
    public void assertTrue() {
        assertEquals(true, true);
    }


    @Test
    public void foo() throws Exception {

        //setup
        Verifier verifier = new Verifier();
        ZipCodeProcessor processor = new ZipCodeProcessor(verifier);


        //enact
        String result1 = processor.process("80302");
        String result2 = processor.process("2345678");
        String result3 = processor.process("321");
        String result4 = processor.process("12234");

        //assert

        assertEquals("Thank you!  Your package will arrive soon.",result1);
        assertEquals("The zip code you entered was the wrong length.",result2);
        assertEquals("The zip code you entered was the wrong length.",result3);
        assertEquals("We're sorry, but the zip code you entered is out of our range.",result4);

    }
}